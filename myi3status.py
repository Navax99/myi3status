#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This script is a simple wrapper which prefixes each i3status line with custom
# information. It is a python reimplementation of:
# http://code.stapelberg.de/git/i3status/tree/contrib/wrapper.pl
#
# To use it, ensure your ~/.i3status.conf contains this line:
#     output_format = "i3bar"
# in the 'general' section.
# Then, in your ~/.i3/config, use:
#     status_command i3status | ~/i3status/contrib/wrapper.py
# In the 'bar' section.
#
# In its current version it will display the cpu frequency governor, but you
# are free to change it to display whatever you like, see the comment in the
# source code below.
#
# © 2012 Valentin Haenel <valentin.haenel@gmx.de>
#
# This program is free software. It comes without any warranty, to the extent
# permitted by applicable law. You can redistribute it and/or modify it under
# the terms of the Do What The Fuck You Want To Public License (WTFPL), Version
# 2, as published by Sam Hocevar. See http://sam.zoy.org/wtfpl/COPYING for more
# details.

import os
import sys
import json
import modules.myi3pomodoro.main as myi3pomodoro
from datetime import timedelta
from threading import Timer

def get_governor():
    """ Get the current governor for cpu0, assuming all CPUs use the same. """
    with open('/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor') as fp:
        return fp.readlines()[0].strip()

def get_uptime():
    with open('/proc/uptime', 'r') as f:
    	uptime_seconds = float(f.readline().split()[0])
    	uptime_string = str(timedelta(seconds = uptime_seconds)).split('.')[0]

    return uptime_string, uptime_seconds

def get_uptime_color(uptime_seconds):
    color = '#FFFFFF'
    pass_color = '#3CFF33'
    alert_color = '#FF0000' #'#FF4D00'
    pass_limit = 8*3600
    alert_limit = 9*3600
    if uptime_seconds > alert_limit:
	color = alert_color
    elif uptime_seconds > pass_limit:
	color = pass_color
        
    return color



#Utils
def print_line(message):
    """ Non-buffered printing to stdout. """
    sys.stdout.write(message + '\n')
    sys.stdout.flush()

def read_line():
    """ Interrupted respecting reader for stdin. """
    # try reading a line, removing any extra whitespace
    try:
        line = sys.stdin.readline().strip()
        # i3status sends EOF, or an empty line
        if not line:
            sys.exit(3)
        return line
    # exit on ctrl-c
    except KeyboardInterrupt:
        sys.exit()

if __name__ == '__main__':
    # Skip the first line which contains the version header.
    print_line(read_line())

    # The second line contains the start of the infinite array.
    print_line(read_line())

    #modules init
    myi3pomodoro.pomodoro_init()

    while True:
        line, prefix = read_line(), ''
        # ignore comma at start of lines
        if line.startswith(','):
            line, prefix = line[1:], ','

        j = json.loads(line)

	# * Uptime
	uptime_string, uptime_seconds = get_uptime()
        j.insert(len(j), {'full_text' : '%s' % uptime_string, 'name' : 'uptime', 'color' : get_uptime_color(uptime_seconds)})
        
	# * Pomodoro
	myi3pomodoro.pomodoro_loop()
	pomodoro_string = myi3pomodoro.get_pomodoro()
	j.insert(len(j), {'full_text' : '%s' % pomodoro_string, 'name' : 'pomodoro', 'color' : myi3pomodoro.get_pomodoro_color()})

        # and echo back new encoded json
        print_line(prefix+json.dumps(j))
