
import os
import sys
import datetime
import json
import sqlite3
from datetime import timedelta
from os.path import expanduser


class Logger(object):
    """ Logger that flush to a unix database """

    def __init__(self):
        home = expanduser("~")
        self.dbfile = home+"/.i3/myi3status/modules/myi3pomodoro/myi3pomodoro.db"
        self.TABLE_NAME = "pomodoros"
        self.initialize()

    def initialize(self):
        self.conn = sqlite3.connect(self.dbfile)
        c = self.conn.cursor()
        #c.execute("CREATE TABLE " + self.TABLE_NAME + " (id INTEGER AUTOINCREMENT, date_start TEXT, date_end TEXT, task_name TEXT")
        try:
        	c.execute("CREATE TABLE "+self.TABLE_NAME+" (id INTEGER PRIMARY KEY AUTOINCREMENT, date_start TEXT NOT NULL, date_end TEXT NOT NULL, task_name TEXT NOT NULL)")
        except:
                pass

    def insert(self,task_name,date_start,date_end):
        c = self.conn.cursor()
        #c.execute("INSERT INTO " + self.TABLE_NAME + " VALUES("+date_start+","+date_end+","+pomodoro_task_name+")")
	c.execute("INSERT INTO "+self.TABLE_NAME+" (date_start,date_end,task_name) VALUES(?,?,?)",(task_name,date_start,date_end))
	self.conn.commit()

    def delete(self):
        pass


pomodoro_logger = Logger()
pomodoro_task_name = "Task dummy"
pomodoro_time = 25*60

def flush_pomodoro():
        global pomodoro_logger, pomodoro_task_name, pomodoro_time
        date_end = datetime.datetime.now()
        date_start = (date_end - datetime.timedelta(seconds=pomodoro_time)).strftime("%Y-%m-%d %H:%M:%S")
        pomodoro_logger.insert(pomodoro_task_name, date_start, date_end.strftime("%Y-%m-%d %H:%M:%S"))

flush_pomodoro()
