#!/usr/bin/python3

import os
import sys
import datetime
import json
import sqlite3
from datetime import timedelta
from os.path import expanduser
from subprocess import Popen

#Pomodoro
class Stopwatch(object):
    """A simple stopwatch class"""
    
    def __init__(self):
        self.start_time = datetime.datetime.now()
        pass
    
    def start(self):
        """Starts the stopwatch """
        self.start_time = datetime.datetime.now()
        return self.start_time
    
    def elapsed(self):
        elapsed = datetime.datetime.now() - self.start_time
        return elapsed.total_seconds()
        

class Timer(object):
    """ A simple timer class"""

    def __init__(self):
        self.timer_time = 0
        self.elapsed = 0
        self.stopwatch = Stopwatch()

    def start(self,time):
        self.timer_time = time #seconds
        self.stopwatch.start()

    def update(self):
        self.elapsed = self.stopwatch.elapsed()
        return self.isdone()
    
    def get_positive_remining(self):
        remining = self.timer_time - self.stopwatch.elapsed()
        return (remining if remining >= 0 else 0)
    
    def isdone(self):
        return self.elapsed >= self.timer_time


class Logger(object):
    """ Logger that flush to a unix database """

    def __init__(self):
        home = expanduser("~")
        self.dbfile = home+"/.i3/myi3status/modules/myi3pomodoro/myi3pomodoro.db"
        self.TABLE_NAME = "pomodoros"
        self.initialize()

    def initialize(self):
        self.conn = sqlite3.connect(self.dbfile)
        c = self.conn.cursor()
        try:
        	#c.execute("CREATE TABLE "+self.TABLE_NAME+" (id INTEGER PRIMARY KEY AUTOINCREMENT, date_start TEXT NOT NULL, date_end TEXT NOT NULL, task_name TEXT NOT NULL)")
        	c.execute("CREATE TABLE "+self.TABLE_NAME+" (date_start TEXT NOT NULL, date_end TEXT NOT NULL, task_name TEXT NOT NULL, PRIMARY KEY (date_start,date_end))")
        except:
                pass

    def insert(self,task_name,date_start,date_end):
        c = self.conn.cursor()
        #c.execute("INSERT INTO " + self.TABLE_NAME + " VALUES("+date_start+","+date_end+","+pomodoro_task_name+")")
	c.execute("INSERT INTO "+self.TABLE_NAME+" (date_start,date_end,task_name) VALUES(?,?,?)",(task_name,date_start,date_end))
	self.conn.commit()

    def delete(self):
        pass
    

pomodoro_time = 0*60 #seconds
pomodoro_task_name = "-: " 
pomodoro_filename = ""
pomodoro_fifo = 0
pomodoro_mode = 0 #0 : stop 1 : work 2,3 : break
pomodoro_timer = Timer()
pomodoro_counter = 0
pomodoro_logger = Logger()

def flush_pomodoro():
        global pomodoro_logger, pomodoro_task_name, pomodoro_time
        date_end = datetime.datetime.now()
        date_start = (date_end - datetime.timedelta(seconds=pomodoro_time)).strftime("%Y-%m-%d %H:%M:%S")
        pomodoro_logger.insert(pomodoro_task_name, date_start, date_end.strftime("%Y-%m-%d %H:%M:%S"))


def pomodoro_proc_action(action_id, task_name):
    global pomodoro_task_name, pomodoro_time, pomodoro_timer, pomodoro_mode
    if action_id == 0: #set name
	pomodoro_task_name = task_name
	pomodoro_time = 0
	pomodoro_mode = 0
        pomodoro_timer.start(pomodoro_time)
    elif action_id == 1: #start pomodoro
	pomodoro_time = 25*60
	pomodoro_mode = 1
	pomodoro_timer.start(pomodoro_time)
    elif action_id == 2: #short break
	pomodoro_time = 5*60
	pomodoro_mode = 2
	pomodoro_timer.start(pomodoro_time)
    elif action_id == 3: #long break
	pomodoro_time = 20*60
	pomodoro_mode = 3
	pomodoro_timer.start(pomodoro_time)

        
def pomodoro_init():
    global pomodoro_filename, pomodoro_fifo
    tmpdir = "/tmp/myi3-pomodoro/"
    try:
	os.mkdir("/tmp/myi3-pomodoro/")
    except:
	pass

    pomodoro_filename = os.path.join(tmpdir,"pomodoro-fifo")
    try:
	os.mkfifo(pomodoro_filename)
    except:
        pass

    #open fifo
    pomodoro_fifo = os.open(pomodoro_filename, os.O_RDONLY | os.O_NONBLOCK)
    
	    
def read_fifo():
    global pomodoro_fifo
    fifo_output = None
    try:
        bufferSize = 512        
        fifo_output = os.read(pomodoro_fifo, bufferSize)        
    except:
        pass
        #print "Unexpected error reading the fifo:", sys.exc_info()[0]        

   
    return fifo_output


def play_sound(action_id):
    player = "aplay"
    args = "-qN"
    home = expanduser("~")
    sounds = [home+"/.emacs.d/myresources/Metal_Gong.wav",
              home+"/.emacs.d/myresources/Computer_Magic.wav"]
    if action_id == 1:
        Popen([player, args, sounds[1]])
    elif action_id == 3 or action_id == 4:
        Popen([player, args, sounds[0]])

        
def schedule_break():
    global pomodoro_timer, pomodoro_mode, pomodoro_counter
    done = pomodoro_timer.isdone()
    if pomodoro_mode != 0:                                    # not stopped
        if done:
    	    if pomodoro_mode == 1:                            # was doing a pomodoro
                flush_pomodoro()                              # flush pomodoro to database
                play_sound(pomodoro_mode)
    		pomodoro_counter = pomodoro_counter + 1
                if pomodoro_counter == 4:                     # it was the 4th
    		    pomodoro_proc_action(3,None)              # long break
    		    pomodoro_counter = 0                      # reset counter
    	        else:
    		    pomodoro_proc_action(2,None)              # short break
    	    elif pomodoro_mode != 0:                          # was doing a break
                play_sound(pomodoro_mode)
    	        pomodoro_proc_action(0,pomodoro_task_name)    # just stop

                
def pomodoro_loop():
    global pomodoro_filename, pomodoro_timer

    fifo_output = read_fifo() 
    try:
       	data = json.loads(fifo_output)
       	action_id = data['id']
        if 'task-name' in data:
            task_name = data['task-name']
        else:
            task_name = None
        pomodoro_proc_action(action_id, task_name)
    except:        
	data = None
        #print "Unexpected error:", sys.exc_info()[0]

    pomodoro_timer.update()
    schedule_break()

    
def get_pomodoro():
    global pomodoro_task_name, pomodoro_timer
    time = pomodoro_timer.get_positive_remining()
    tmp = str(timedelta(seconds = time)).split('.')[0]
    return pomodoro_task_name+": "+tmp


def get_pomodoro_color():
    work_color = '#3CFF33'
    break_color = '#00FFFF'
    idle_color = '#FFFFFF'
    if pomodoro_mode == 0:
	color = idle_color
    elif pomodoro_mode == 1:
	color = work_color
    else:
	color = break_color
    return color


